import * as dotenv from 'dotenv';
dotenv.config();

import express, { Request, Response, Express } from 'express';
import bodyParser from 'body-parser';
import config from './configs/app';
import db from './configs/db';
import { NodeEnv } from './types/environment';

const app: Express = express();
const port: number = config.port || 3030;
const env: NodeEnv = config.env || 'development';

app.use(bodyParser.json()); // To support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // To support URL-encoded bodies
    extended: true
  })
);

app.get('/', (req: Request, res: Response) => {
  res.send('Hello World');
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
  console.log(`Environment: ${env}`);
  console.log(`DB Host: ${db[env].host}`);
});
