export default {
  local: {
    host: '127.0.0.1'
  },
  development: {
    host: '127.0.0.2'
  },
  production: {
    host: '127.0.0.3'
  }
};
